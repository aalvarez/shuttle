/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef VIRTUALNETWORK_H
#define VIRTUALNETWORK_H

#include "cluster/Raft.h"
#include "cluster/Rpc.h"

class VirtualNetwork;
class VirtualRpc;
class VirtualCluster;


class VirtualHost {
public:
    typedef boost::shared_ptr<VirtualHost> ptr_t;

    fts::cluster::Raft::ptr_t raft;
    fts::cluster::Cluster::ptr_t cluster;
    fts::cluster::Rpc::ptr_t rpc;

    VirtualHost(fts::cluster::Cluster::ptr_t cluster, fts::cluster::Rpc::ptr_t rpc);
    ~VirtualHost();
};


class VirtualNetwork {
private:
    mutable boost::recursive_mutex mutex;
    std::list<VirtualHost::ptr_t> hosts;
    std::list<fts::cluster::Node> workers, masters;

public:
    VirtualNetwork();

    ~VirtualNetwork();

    VirtualHost::ptr_t addHost(const std::string &id, fts::cluster::Node::Type type);

    void removeHost(VirtualHost::ptr_t host);

    void sendHeartbeat(const fts::cluster::Node &node);

    void sendLeaderBeat(const std::string &id, uint64_t term);

    void requestVoting(const std::string &id, uint64_t term);

    void vote(const std::string &id, bool vote);

    std::list<fts::cluster::Node> getWorkers() const;
    std::list<fts::cluster::Node> getMasters() const;
};


#endif // VIRTUALNETWORK_H
