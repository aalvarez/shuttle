/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <boost/test/included/unit_test.hpp>
#include <boost/thread.hpp>
#include "cluster/Raft.h"

#include "VirtualNetwork.h"


BOOST_AUTO_TEST_SUITE(cluster)
BOOST_AUTO_TEST_SUITE(Raft)


// One single worker on the cluster, there must be no leader attempt
BOOST_FIXTURE_TEST_CASE(SingleWorker, VirtualNetwork)
{
    VirtualHost::ptr_t node = this->addHost("node1.cern.ch", fts::cluster::Node::kWorker);

    boost::this_thread::sleep(fts::cluster::Raft::kMaxElectionTimeout);

    BOOST_CHECK_EQUAL(node->cluster->getSelf().type, fts::cluster::Node::kWorker);
    BOOST_CHECK(node->raft->getLeader().empty());
}


// One single master on the cluster, who must become the leader
BOOST_FIXTURE_TEST_CASE(SingleMaster, VirtualNetwork)
{
    VirtualHost::ptr_t node = this->addHost("node1.cern.ch", fts::cluster::Node::kBackup);

    boost::this_thread::sleep(fts::cluster::Raft::kMaxElectionTimeout);

    BOOST_CHECK_EQUAL(node->cluster->getSelf().type, fts::cluster::Node::kLeader);
    BOOST_CHECK_EQUAL(node->raft->getLeader(), "node1.cern.ch");
}


// Two masters
BOOST_FIXTURE_TEST_CASE(TwoMasters, VirtualNetwork)
{
    VirtualHost::ptr_t node1 = this->addHost("node1.cern.ch", fts::cluster::Node::kBackup);
    VirtualHost::ptr_t node2 = this->addHost("node2.cern.ch", fts::cluster::Node::kBackup);

    boost::this_thread::sleep(fts::cluster::Raft::kMaxElectionTimeout);

    BOOST_CHECK(
        (node1->cluster->getSelf().type == fts::cluster::Node::kLeader) ^
        (node2->cluster->getSelf().type == fts::cluster::Node::kLeader)
    );

    BOOST_CHECK_EQUAL(node1->raft->getLeader(), node2->raft->getLeader());
}


// Two masters, a third one enters with the cluster already formed
BOOST_FIXTURE_TEST_CASE(TwoMastersLateArrival, VirtualNetwork)
{
    VirtualHost::ptr_t node1 = this->addHost("node1.cern.ch", fts::cluster::Node::kBackup);
    VirtualHost::ptr_t node2 = this->addHost("node2.cern.ch", fts::cluster::Node::kBackup);

    boost::this_thread::sleep(fts::cluster::Raft::kMaxElectionTimeout);

    BOOST_CHECK(
        (node1->cluster->getSelf().type == fts::cluster::Node::kLeader) ^
        (node2->cluster->getSelf().type == fts::cluster::Node::kLeader)
    );

    BOOST_CHECK_EQUAL(node1->raft->getLeader(), node2->raft->getLeader());
    std::string leader = node1->raft->getLeader();

    VirtualHost::ptr_t node3 = this->addHost("node3.cern.ch", fts::cluster::Node::kBackup);
    boost::this_thread::sleep(fts::cluster::Raft::kMaxElectionTimeout);

    BOOST_CHECK_EQUAL(leader, node3->raft->getLeader());
    BOOST_CHECK_EQUAL(node1->raft->getCurrentTerm(), node3->raft->getCurrentTerm());
}


// Three masters. Leader dies, so other must take over
BOOST_FIXTURE_TEST_CASE(LeaderDies, VirtualNetwork)
{
    std::map<std::string, VirtualHost::ptr_t> hosts;
    hosts["node1.cern.ch"] = this->addHost("node1.cern.ch", fts::cluster::Node::kBackup);
    hosts["node2.cern.ch"] = this->addHost("node2.cern.ch", fts::cluster::Node::kBackup);
    hosts["node3.cern.ch"] = this->addHost("node3.cern.ch", fts::cluster::Node::kBackup);

    boost::this_thread::sleep(fts::cluster::Raft::kMaxElectionTimeout);

    std::string leader = hosts["node1.cern.ch"]->raft->getLeader();

    for (auto i = hosts.begin(); i != hosts.end(); ++i) {
        BOOST_CHECK_EQUAL(leader, i->second->raft->getLeader());
    }

    // Wait a while, the same leader should be
    boost::this_thread::sleep(fts::cluster::Raft::kMaxElectionTimeout);

    for (auto i = hosts.begin(); i != hosts.end(); ++i) {
        BOOST_CHECK_EQUAL(leader, i->second->raft->getLeader());
    }

    // Kill the one that is the leader
    this->removeHost(hosts[leader]);
    hosts.erase(leader);

    // Give it time
    boost::this_thread::sleep(fts::cluster::Raft::kMaxElectionTimeout);
    boost::this_thread::sleep(fts::cluster::Raft::kMaxElectionTimeout);

    // There should be a new leader
    std::string newLeader = hosts.begin()->second->raft->getLeader();
    BOOST_CHECK_NE(newLeader, leader);

    for (auto i = hosts.begin(); i != hosts.end(); ++i) {
        BOOST_CHECK_EQUAL(newLeader, i->second->raft->getLeader());
    }
}


BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
