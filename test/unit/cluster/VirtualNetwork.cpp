/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <boost/test/included/unit_test.hpp>
#include <boost/thread.hpp>
#include "VirtualNetwork.h"


class VirtualCluster: public fts::cluster::Cluster {
public:
    fts::cluster::Node self;
    VirtualNetwork *network;

    VirtualCluster(const std::string &selfId, fts::cluster::Node::Type selfType, VirtualNetwork *network):
        network(network) {
        self.id = selfId;
        self.type = selfType;
    }

    std::list<fts::cluster::Node> getWorkers() const {
        return network->getWorkers();
    }

    std::list<fts::cluster::Node> getMasters() const {
        return network->getMasters();
    }

    fts::cluster::Node getSelf() const {
        return self;
    }

    void setSelfType(fts::cluster::Node::Type type) {
        self.type = type;
    }
};


class VirtualRpc: public fts::cluster::Rpc {
private:
    VirtualNetwork *network;

public:

    VirtualRpc(VirtualNetwork *network): network(network) {

    }

    void sendHeartbeat(const fts::cluster::Node &node) {
        network->sendHeartbeat(node);
    }

    void sendLeaderBeat(const std::string &id, uint64_t term) {
        network->sendLeaderBeat(id, term);
    }

    void requestVoting(const std::string &id, uint64_t term) {
        network->requestVoting(id, term);
    }

    void vote(const std::string &id, bool vote) {
        network->vote(id, vote);
    }
};


VirtualHost::VirtualHost(fts::cluster::Cluster::ptr_t cluster, fts::cluster::Rpc::ptr_t rpc):
    raft(new fts::cluster::Raft(cluster, rpc)), cluster(cluster), rpc(rpc)
{
}


VirtualHost::~VirtualHost()
{
    raft->stop();
}


VirtualNetwork::VirtualNetwork()
{
}


VirtualNetwork::~VirtualNetwork()
{
    for (auto i = hosts.begin(); i != hosts.end(); ++i) {
        VirtualHost::ptr_t &host = *i;
        host->raft->stop();
    }
}


VirtualHost::ptr_t VirtualNetwork::addHost(const std::string &id, fts::cluster::Node::Type type)
{
    boost::lock_guard<boost::recursive_mutex> lock(mutex);

    fts::cluster::Cluster::ptr_t cluster(new VirtualCluster(id, type, this));
    fts::cluster::Rpc::ptr_t rpc(new VirtualRpc(this));
    VirtualHost::ptr_t host(new VirtualHost(cluster, rpc));
    hosts.push_back(host);

    fts::cluster::Node node;
    node.id = id;
    node.type = type;
    if (type == fts::cluster::Node::kWorker) {
        workers.push_back(node);
    }
    else {
        masters.push_back(node);
    }

    return host;
}


void VirtualNetwork::removeHost(VirtualHost::ptr_t toBeRemoved)
{
    boost::lock_guard<boost::recursive_mutex> lock(mutex);
    for (auto i = hosts.begin(); i != hosts.end(); ++i) {
        VirtualHost::ptr_t &host = *i;
        if (host == toBeRemoved) {
            hosts.erase(i);
            break;
        }
    }
    toBeRemoved->raft->stop();
}


void VirtualNetwork::sendHeartbeat(const fts::cluster::Node &node)
{
    boost::lock_guard<boost::recursive_mutex> lock(mutex);
    for (auto i = hosts.begin(); i != hosts.end(); ++i) {
        VirtualHost::ptr_t &host = *i;
        host->rpc->getClusterDiscoveryListener()->notifyNode(node);
    }
}


void VirtualNetwork::sendLeaderBeat(const std::string &id, uint64_t term)
{
    boost::lock_guard<boost::recursive_mutex> lock(mutex);
    for (auto i = hosts.begin(); i != hosts.end(); ++i) {
        VirtualHost::ptr_t &host = *i;
        host->rpc->getRaftListener()->notifyLeaderBeat(id, term);
    }
}


void VirtualNetwork::requestVoting(const std::string &id, uint64_t term)
{
    boost::lock_guard<boost::recursive_mutex> lock(mutex);
    for (auto i = hosts.begin(); i != hosts.end(); ++i) {
        VirtualHost::ptr_t &host = *i;
        host->rpc->getRaftListener()->notifyVoteRequest(id, term);
    }
}


void VirtualNetwork::vote(const std::string &id, bool vote)
{
    boost::lock_guard<boost::recursive_mutex> lock(mutex);
    for (auto i = hosts.begin(); i != hosts.end(); ++i) {
        VirtualHost::ptr_t &host = *i;
        host->rpc->getRaftListener()->notifyVote(id, vote);
    }
}


std::list<fts::cluster::Node> VirtualNetwork::getWorkers() const
{
    boost::lock_guard<boost::recursive_mutex> lock(mutex);
    return workers;
}


std::list<fts::cluster::Node> VirtualNetwork::getMasters() const
{
    boost::lock_guard<boost::recursive_mutex> lock(mutex);
    return masters;
}
