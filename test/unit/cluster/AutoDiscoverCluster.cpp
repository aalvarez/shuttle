/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <boost/test/included/unit_test.hpp>
#include "cluster/AutoDiscoverCluster.h"


BOOST_AUTO_TEST_SUITE(cluster)
BOOST_AUTO_TEST_SUITE(AutoDiscover)

struct NullDeleter {template<typename T> void operator()(T*) {} };


class MockRpc: public fts::cluster::Rpc {
protected:
    fts::cluster::Node self;

public:


    void sendHeartbeat(const fts::cluster::Node &node) {
        self = node;
        // Need to pass back
        clusterDiscovery->notifyNode(node);
    }

    void sendLeaderBeat(const std::string &, uint64_t) {
        // Pass
    }

    void requestVoting(const std::string&, uint64_t) {
        // Pass
    }

    void vote(const std::string&, bool) {
        // Pass
    }
};

/// Simple case where there is a single node
BOOST_FIXTURE_TEST_CASE(SingleNode, MockRpc)
{
    fts::cluster::Rpc::ptr_t ptr(this, NullDeleter());
    fts::cluster::AutoDiscoverCluster cluster(ptr, false);

    BOOST_CHECK_NE((void*)clusterDiscovery, (void*)NULL);

    // Give a chance for the autodiscover to notify itself
    boost::this_thread::sleep(fts::cluster::AutoDiscoverCluster::kHeartBeatInterval);
    boost::this_thread::sleep(fts::cluster::AutoDiscoverCluster::kHeartBeatInterval);

    // Must have received it
    BOOST_CHECK_NE(self.id.size(), 0);
    BOOST_CHECK_EQUAL(self.id, cluster.getSelf().id);
}

/// This time, we add an extra node
BOOST_FIXTURE_TEST_CASE(TwoNodes, MockRpc)
{
    fts::cluster::Rpc::ptr_t ptr(this, NullDeleter());
    fts::cluster::AutoDiscoverCluster cluster(ptr, false);

    BOOST_CHECK_NE((void*)clusterDiscovery, (void*)NULL);

    boost::this_thread::sleep(fts::cluster::AutoDiscoverCluster::kHeartBeatInterval);

    fts::cluster::Node extraNode;
    extraNode.id = "fakenode";
    extraNode.type = fts::cluster::Node::kWorker;
    extraNode.beat = time(NULL);
    clusterDiscovery->notifyNode(extraNode);

    boost::this_thread::sleep(fts::cluster::AutoDiscoverCluster::kHeartBeatInterval);

    // Must have received it
    BOOST_CHECK_NE(self.id.size(), 0);
    BOOST_CHECK_EQUAL(self.id, cluster.getSelf().id);

    // The discovery cluster must have received ours
    BOOST_CHECK_EQUAL(cluster.getMasters().size(), 1);
    BOOST_CHECK_EQUAL(cluster.getWorkers().size(), 1);
}

// Send the beat twice. It still should appear only once
BOOST_FIXTURE_TEST_CASE(NoDuplications, MockRpc)
{
    fts::cluster::Rpc::ptr_t ptr(this, NullDeleter());
    fts::cluster::AutoDiscoverCluster cluster(ptr, false);

    BOOST_CHECK_NE((void*)clusterDiscovery, (void*)NULL);

    fts::cluster::Node extraNode;
    extraNode.id = "fakenode";
    extraNode.type = fts::cluster::Node::kWorker;
    extraNode.beat = time(NULL);
    clusterDiscovery->notifyNode(extraNode);

    boost::this_thread::sleep(fts::cluster::AutoDiscoverCluster::kHeartBeatInterval);

    extraNode.beat = time(NULL);
    clusterDiscovery->notifyNode(extraNode);

    // The discovery cluster must have received ours
    BOOST_CHECK_EQUAL(cluster.getMasters().size(), 1);
    BOOST_CHECK_EQUAL(cluster.getWorkers().size(), 1);

    fts::cluster::Node worker = *cluster.getWorkers().begin();
    BOOST_CHECK_EQUAL(extraNode.beat, worker.beat);
}

// Old entries must be purged
BOOST_FIXTURE_TEST_CASE(PurgeOldEntries, MockRpc)
{
    fts::cluster::Rpc::ptr_t ptr(this, NullDeleter());
    fts::cluster::AutoDiscoverCluster cluster(ptr, false);

    BOOST_CHECK_NE((void *) clusterDiscovery, (void *) NULL);

    fts::cluster::Node extraNode;
    extraNode.id = "fakenode";
    extraNode.type = fts::cluster::Node::kWorker;
    extraNode.beat = time(NULL) - 3600;
    clusterDiscovery->notifyNode(extraNode);

    boost::this_thread::sleep(fts::cluster::AutoDiscoverCluster::kPurgeTimeout);
    boost::this_thread::sleep(fts::cluster::AutoDiscoverCluster::kHeartBeatInterval);

    // It should be gone by now
    BOOST_CHECK_EQUAL(cluster.getMasters().size(), 1);
    BOOST_CHECK_EQUAL(cluster.getWorkers().size(), 0);
}

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE_END()
