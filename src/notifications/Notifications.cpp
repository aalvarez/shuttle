/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Notifications.h"

#include <fstream>

#include <cajun/json/elements.h>
#include <cajun/json/writer.h>
#include <linux/limits.h>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>


namespace fts3 {


static std::string ReplaceNonPrintableCharacters(const std::string &s)
{
    std::string result;
    result.reserve(s.size());

    for (auto c = s.begin(); c != s.end(); ++c) {
        if (*c < 32 || *c > 126) {
            result.append(" ");
        }
        else {
            result += *c;
        }
    }
    return result;
}


static void safeSetMetadata(json::Object &json, const std::string &key, const std::string &value)
{
    try {
        std::istringstream valueStream(value);
        json::UnknownElement metadata;
        json::Reader::Read(metadata, valueStream);
        json[key] = metadata;
    }
    catch (...) {
        json[key] = json::String(value);
    }
}


static void writeToSpoolDir(const std::string &baseDir, DirQ &dirq, const json::Object &body, const std::string &vo,
    long ttl, const std::string &destination)
{
    std::stringstream serializedBody;
    json::Writer::Write(body, serializedBody);

    json::Object headers;
    headers["persistent"] = json::String("true");
    headers["ttl"] = json::String(boost::lexical_cast<std::string>(ttl));
    headers["destination"] = json::String(destination);
    headers["vo"] = json::String(vo);

    json::Object message;
    message["body"] = json::String(serializedBody.str());
    message["text"] = json::Boolean(true);
    message["header"] = headers;

    char tempName[PATH_MAX];
    snprintf(tempName, PATH_MAX, "%s/XXXXXX", baseDir.c_str());

    int tempFd = mkstemp(tempName);
    if (tempFd < 0) {
        std::ostringstream errStr;
        errStr << "Coult not create temporary file for notification: " << errno;
        throw SystemError(errStr.str());
    }
    close(tempFd);

    std::ofstream serializedMessage(tempName);

    json::Writer::Write(message, serializedMessage);
    serializedMessage.flush();
    serializedMessage.close();

    if (dirq_add_path(dirq, tempName) == NULL) {
        std::ostringstream errStr;
        errStr << "Could not add notification to dirq: " << dirq_get_errstr(dirq);
    }
}


Notifications::Notifications(const NotificationParameters &params):
    dirQueueHandle(params.spoolDirectory), parameters(params)
{
    // Prepend /topic or /queue depending on destinationAreTopics
    std::string prefix;
    if (parameters.destinationAreTopics) {
        prefix = "/topic/";
    }
    else {
        prefix = "/queue/";
    }

    parameters.terminalDestination = prefix + parameters.terminalDestination;
    parameters.stateDestination = prefix + parameters.stateDestination;
    parameters.startDestination = prefix + parameters.startDestination;
}


Notifications::~Notifications()
{
}


void Notifications::sendTransferTerminal(const fts3::events::TransferCompleteEvent &transferEnd)
{
    json::Object body;

    body["tr_id"] = json::String(transferEnd.transfer_id());
    body["endpnt"] = json::String(transferEnd.agent().endpoint());
    body["src_srm_v"] = json::String(transferEnd.pair().source().srm_version());
    body["dest_srm_v"] = json::String(transferEnd.pair().destination().srm_version());
    body["vo"] = json::String(transferEnd.vo_name());
    body["vo_name"] = json::String(transferEnd.vo_name());
    body["src_url"] = json::String(transferEnd.pair().source().surl());
    body["dst_url"] = json::String(transferEnd.pair().destination().surl());
    body["src_hostname"] = json::String(transferEnd.pair().source().hostname());
    body["dst_hostname"] = json::String(transferEnd.pair().destination().hostname());
    body["src_site_name"] = json::String(transferEnd.pair().source().site_name());
    body["dst_site_name"] = json::String(transferEnd.pair().destination().site_name());
    body["t_channel"] = json::String(transferEnd.pair().name());
    body["timestamp_tr_st"] = json::Number(transferEnd.transfer().start());
    body["timestamp_tr_comp"] = json::Number(transferEnd.transfer().end());
    body["timestamp_chk_src_st"] = json::Number(transferEnd.source_checksum().start());
    body["timestamp_chk_src_ended"] = json::Number(transferEnd.source_checksum().end());
    body["timestamp_checksum_dest_st"] = json::Number(transferEnd.dest_checksum().start());
    body["timestamp_checksum_dest_ended"] = json::Number(transferEnd.dest_checksum().end());
    body["t_timeout"] = json::Number(transferEnd.transfer_timeout());
    body["chk_timeout"] = json::Number(transferEnd.checksum_timeout());
    body["t_error_code"] = json::Number(transferEnd.transfer_error_code());
    body["tr_error_scope"] = json::String(transferEnd.transfer_error_scope());
    body["t_failure_phase"] = json::String(transferEnd.failure_phase());
    body["tr_error_category"] = json::String(transferEnd.transfer_error_category());
    body["t_final_transfer_state"] = json::String(transferEnd.final_transfer_state());
    body["tr_bt_transfered"] = json::Number(transferEnd.total_bytes_transfered());
    body["nstreams"] = json::Number(transferEnd.number_of_streams());
    body["buf_size"] = json::Number(transferEnd.tcp_buffer_size());
    body["tcp_buf_size"] = json::Number(transferEnd.tcp_buffer_size());
    body["block_size"] = json::Number(transferEnd.block_size());
    body["f_size"] = json::Number(transferEnd.file_size());
    body["time_srm_prep_st"] = json::Number(transferEnd.srm_preparation().start());
    body["time_srm_prep_end"] = json::Number(transferEnd.srm_preparation().end());
    body["time_srm_fin_st"] = json::Number(transferEnd.srm_finalization().start());
    body["time_srm_fin_end"] = json::Number(transferEnd.srm_finalization().end());
    body["srm_space_token_src"] = json::String(transferEnd.pair().source().srm_space_token());
    body["srm_space_token_dst"] = json::String(transferEnd.pair().destination().srm_space_token());

    std::string temp = ReplaceNonPrintableCharacters(transferEnd.transfer_error_message());
    temp.erase(std::remove(temp.begin(), temp.end(), '\n'), temp.end());
    temp.erase(std::remove(temp.begin(), temp.end(), '\''), temp.end());
    temp.erase(std::remove(temp.begin(), temp.end(), '\"'), temp.end());
    if (temp.size() > 1024) {
        temp.erase(1024);
    }

    body["t__error_message"] = json::String(temp);
    body["tr_timestamp_start"] = json::Number(transferEnd.total_process().start());
    body["tr_timestamp_complete"] = json::Number(transferEnd.total_process().end());

    body["channel_type"] = json::String(transferEnd.pair().type());
    body["user_dn"] = json::String(transferEnd.user_dn());

    if (transferEnd.has_file_metadata()) {
        safeSetMetadata(body, "file_metadata", transferEnd.file_metadata());
    }
    else {
        body["file_metadata"] = json::String();
    }

    if (transferEnd.has_job_metadata()) {
        safeSetMetadata(body, "job_metadata", transferEnd.job_metadata());
    }
    else {
        body["job_metadata"] = json::String();
    }

    writeToSpoolDir(parameters.spoolDirectory, dirQueueHandle, body, transferEnd.vo_name(),
        parameters.ttl.total_milliseconds(), parameters.terminalDestination);
}


void Notifications::sendTransferStart(const fts3::events::TransferStartEvent &transferStart)
{
    json::Object body;

    body["agent_fqdn"] = json::String(transferStart.agent().fqdn());
    body["transfer_id"] = json::String(transferStart.transfer_id());
    body["endpnt"] = json::String(transferStart.agent().endpoint());
    body["timestamp"] = json::Number(transferStart.timestamp());
    body["src_srm_v"] = json::String(transferStart.pair().source().srm_version());
    body["dest_srm_v"] = json::String(transferStart.pair().destination().srm_version());
    body["vo"] = json::String(transferStart.vo_name());
    body["vo_name"] = json::String(transferStart.vo_name());
    body["src_url"] = json::String(transferStart.pair().source().surl());
    body["dst_url"] = json::String(transferStart.pair().destination().surl());
    body["src_hostname"] = json::String(transferStart.pair().source().hostname());
    body["dst_hostname"] = json::String(transferStart.pair().destination().hostname());
    body["src_site_name"] = json::String(transferStart.pair().source().site_name());
    body["dst_site_name"] = json::String(transferStart.pair().destination().site_name());
    body["t_channel"] = json::String(transferStart.pair().name());
    body["srm_space_token_src"] = json::String(transferStart.pair().source().srm_space_token());
    body["srm_space_token_dst"] = json::String(transferStart.pair().destination().srm_space_token());
    body["user_dn"] = json::String(transferStart.user_dn());

    if (transferStart.has_file_metadata()) {
        safeSetMetadata(body, "file_metadata", transferStart.file_metadata());
    }
    else {
        body["file_metadata"] = json::String();
    }

    if (transferStart.has_job_metadata()) {
        safeSetMetadata(body, "job_metadata", transferStart.job_metadata());
    }
    else {
        body["job_metadata"] = json::String();
    }

    writeToSpoolDir(parameters.spoolDirectory, dirQueueHandle, body, transferStart.vo_name(),
        parameters.ttl.total_milliseconds(), parameters.startDestination);
}


void Notifications::sendTransferState(const fts3::events::StateChangeEvent &stateChange)
{
    json::Object body;

    body["endpnt"] = json::String(stateChange.endpoint());
    body["user_dn"] = json::String(stateChange.user_dn());
    body["src_url"] = json::String(stateChange.source_url());
    body["dst_url"] = json::String(stateChange.dest_url());
    body["vo_name"] = json::String(stateChange.vo_name());
    body["source_se"] = json::String(stateChange.source_se());
    body["dest_se"] = json::String(stateChange.dest_se());
    body["job_id"] = json::String(stateChange.job_id());
    body["file_id"] = json::Number(stateChange.file_id());
    body["job_state"] = json::String(stateChange.job_state());
    body["file_state"] = json::String(stateChange.file_state());
    body["retry_counter"] = json::Number(stateChange.retry_counter());
    body["retry_max"] = json::Number(stateChange.retry_max());

    if (stateChange.has_job_metadata()) {
        safeSetMetadata(body, "job_metadata", stateChange.job_metadata());
    }
    else {
        body["job_metadata"] = json::String();
    }

    if (stateChange.has_file_metadata()) {
        safeSetMetadata(body, "file_metadata", stateChange.file_metadata());
    }
    else {
        body["file_metadata"] = json::String();
    }

    body["timestamp"] = json::Number(stateChange.timestamp());

    writeToSpoolDir(parameters.spoolDirectory, dirQueueHandle, body, stateChange.vo_name(),
        parameters.ttl.total_milliseconds(), parameters.stateDestination);
}


void Notifications::purge(void)
{
    dirq_purge(dirQueueHandle);
}


} // end namespace fts3
