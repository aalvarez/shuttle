/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NOTIFICATIONS_H
#define NOTIFICATIONS_H

#include "msg-bus/events/TransferCompleteEvent.pb.h"
#include "msg-bus/events/TransferStartEvent.pb.h"
#include "msg-bus/events/StateChangeEvent.pb.h"

#include "DirQ.h"

#include <boost/date_time/posix_time/posix_time_types.hpp>


namespace fts3 {

struct NotificationParameters {
    std::string spoolDirectory;

    boost::posix_time::time_duration ttl;

    bool destinationAreTopics;
    std::string terminalDestination;
    std::string startDestination;
    std::string stateDestination;
};

/// This subsystem produces the JSON messages that will be sent to the
/// ActiveMQ broker. It uses dirq as the messaging implementation
/// fts_msg_bulk wraps stompclt, which acts as consumes the messages and send them
/// to the external broker
class Notifications {
private:
    DirQ dirQueueHandle;
    NotificationParameters parameters;

public:

    /// Constructor
    Notifications(const NotificationParameters &parameters);

    /// Destructor
    ~Notifications();

    /// Send a transfer terminal message
    void sendTransferTerminal(const fts3::events::TransferCompleteEvent&);

    /// Send a transfer start message
    void sendTransferStart(const fts3::events::TransferStartEvent&);

    /// Send a state change message
    void sendTransferState(const fts3::events::StateChangeEvent&);

    /// Purge directory queue
    void purge(void);
};

}

#endif // NOTIFICATIONS_H
