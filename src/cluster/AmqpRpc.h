/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AMQPRPC_H
#define AMQPRPC_H

#include "Rpc.h"
#include <string>
#include <externals/SimpleAmqpClient/src/SimpleAmqpClient/Channel.h>
#include <boost/thread.hpp>


namespace fts {
namespace cluster {

/// Connection parameters required for AMQP
struct AmqpConnectionParameters {
    std::string host;
    unsigned port;
    std::string username;
    std::string password;
    std::string vhost;
};


/// An implementation of the RPC mechanism required by FTS clustering using AMQP
class AmqpRpc: public Rpc {
private:
    AmqpClient::Channel::ptr_t rpcProducer, rpcConsumer;
    std::string rpcConsumerTag;
    boost::scoped_ptr<boost::thread> consumerThread;
    boost::mutex mutex;

    void serveCalls(void);

public:
    AmqpRpc(const AmqpConnectionParameters &params);

    ~AmqpRpc();

    void sendHeartbeat(const Node &node);

    void sendLeaderBeat(const std::string &id, uint64_t term);

    void requestVoting(const std::string &id, uint64_t term);

    void vote(const std::string &id, bool vote);
};

} // end namespace cluster
} // end namespace fts


#endif // AMQPRPC_H
