/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RPC_H
#define RPC_H

#include "Cluster.h"
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>


namespace fts {
namespace cluster {

/// To be implemented by those who want to receive notification of nodes on the cluster
class ClusterDiscoveryListener {
public:
    virtual ~ClusterDiscoveryListener() {};
    virtual void notifyNode(const Node &node) = 0;
};

/// To be implemented by the Raft implementation
class RaftListener {
public:
    virtual ~RaftListener() {};

    virtual void notifyLeaderBeat(const std::string &id, uint64_t term) = 0;
    virtual void notifyVoteRequest(const std::string &id, uint64_t term) = 0;
    virtual void notifyVote(const std::string &id, bool vote) = 0;
};

/// Abstraction over the RPC mechanism used
class Rpc {
protected:
    ClusterDiscoveryListener *clusterDiscovery;
    RaftListener *raft;

public:
    typedef boost::shared_ptr<Rpc> ptr_t;

    Rpc(): clusterDiscovery(NULL), raft(NULL) {
    }

    virtual ~Rpc() {
    }

    virtual void setClusterDiscoveryListener(ClusterDiscoveryListener *cd) {
        clusterDiscovery = cd;
    }

    virtual void setRaftListener(RaftListener *rft) {
        raft = rft;
    }

    virtual ClusterDiscoveryListener* getClusterDiscoveryListener(void) {
        return clusterDiscovery;
    }

    virtual RaftListener* getRaftListener(void) {
        return raft;
    }

    virtual void sendHeartbeat(const Node &node) = 0;

    virtual void sendLeaderBeat(const std::string &id, uint64_t term) = 0;

    virtual void requestVoting(const std::string &id, uint64_t term) = 0;

    virtual void vote(const std::string &id, bool vote) = 0;
};

} // end namespace cluster
} // end namespace fts

#endif // RPC_H
