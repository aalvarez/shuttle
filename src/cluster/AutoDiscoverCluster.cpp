/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "AutoDiscoverCluster.h"
#include <boost/asio/ip/host_name.hpp>
#include <boost/interprocess/detail/os_thread_functions.hpp>
#include <common/Logger.h>


namespace fts {
namespace cluster {

const boost::posix_time::time_duration AutoDiscoverCluster::kHeartBeatInterval = boost::posix_time::seconds(2);
const boost::posix_time::time_duration AutoDiscoverCluster::kPurgeTimeout = boost::posix_time::seconds(20);


class AutoDiscoverProcess: public ClusterDiscoveryListener {
private:
    Rpc::ptr_t rpc;
    boost::thread_group threads;
    Node self;
    std::list<Node> workers, masters;
    mutable boost::recursive_mutex mutex;

    void purge(std::list<Node> &members)
    {
        int now = time(NULL);

        for (auto i = members.begin(); i != members.end();) {
            int diff = now - i->beat;
            if (boost::posix_time::seconds(diff) > AutoDiscoverCluster::kPurgeTimeout) {
                members.erase(i++);
            }
            else {
                ++i;
            }
        }
    }

    void updateMembers(std::list<Node> &members, const Node &node)
    {
        bool found = false;

        for (auto i = members.begin(); i != members.end();) {
            if (node.id == i->id) {
                found = true;
                *i = node;
                ++i;
            }
            else if (node.type == Node::kLeader && i->type == Node::kLeader) {
                i->type = Node::kBackup;
                ++i;
            }
            else {
                ++i;
            }
        }

        if (!found) {
            members.push_back(node);
        }
    }

    void heartbeatImpl() {
        while (!boost::this_thread::interruption_requested()) {
            try {
                {
                    boost::lock_guard<boost::recursive_mutex> lock(mutex);
                    self.beat = time(NULL);
                    rpc->sendHeartbeat(self);
                }
                boost::this_thread::sleep(AutoDiscoverCluster::kHeartBeatInterval);
            }
            catch (const boost::thread_interrupted&) {
                break;
            }
            catch (const std::exception &e) {
                FTS3_COMMON_LOGGER_NEWLOG(ERR) << e.what() << fts3::common::commit;
            }
            catch (...) {
                FTS3_COMMON_LOGGER_NEWLOG(ERR) << "Unexpected exception" << fts3::common::commit;
            }
        }
    }

public:
    AutoDiscoverProcess(Rpc::ptr_t rpc, bool isDummy): rpc(rpc) {
        self.id = generateId();
        if (isDummy) {
            self.type = Node::kWorker;
        }
        else {
            self.type = Node::kBackup;
        }
        threads.create_thread(boost::bind(&AutoDiscoverProcess::heartbeatImpl, this));
        rpc->setClusterDiscoveryListener(this);
    }

    ~AutoDiscoverProcess() {
        threads.interrupt_all();
        threads.join_all();
    }

    std::string generateId() {
        std::ostringstream idStream;
        idStream << boost::asio::ip::host_name() << "::"
            << boost::interprocess::ipcdetail::get_current_process_id() << "::"
            << boost::this_thread::get_id();
        return idStream.str();
    }

    std::list<Node> getWorkers() const {
        boost::lock_guard<boost::recursive_mutex> lock(mutex);
        return workers;
    }

    std::list<Node> getMasters() const {
        boost::lock_guard<boost::recursive_mutex> lock(mutex);
        return masters;
    }

    void notifyNode(const Node &node) {
        boost::lock_guard<boost::recursive_mutex> lock(mutex);
        if (node.type == Node::kWorker) {
            updateMembers(workers, node);
        }
        else {
            updateMembers(masters, node);
        }
        purge(workers);
        purge(masters);
        FTS3_COMMON_LOGGER_NEWLOG(TRACE) << "Received hearbeat. Found "
            << workers.size() << " workers and " << masters.size() << " masters in the cluster"
            << fts3::common::commit;
    }

    Node getSelf() const {
        boost::lock_guard<boost::recursive_mutex> lock(mutex);
        return self;
    }

    void setSelfType(Node::Type type) {
        boost::lock_guard<boost::recursive_mutex> lock(mutex);
        self.type = type;
        rpc->sendHeartbeat(self);
    }

};



AutoDiscoverCluster::AutoDiscoverCluster(Rpc::ptr_t rpc, bool isDummy):
    process(new AutoDiscoverProcess(rpc, isDummy))
{
}


AutoDiscoverCluster::~AutoDiscoverCluster()
{
}


std::list<Node> AutoDiscoverCluster::getWorkers() const {
    return process->getWorkers();
}


std::list<Node> AutoDiscoverCluster::getMasters() const {
    return process->getMasters();
}


Node AutoDiscoverCluster::getSelf() const
{
    return process->getSelf();
}


void AutoDiscoverCluster::setSelfType(Node::Type type)
{
    process->setSelfType(type);
}

} // end namespace cluster
} // end namespace fts
