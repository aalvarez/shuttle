/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AUTODISCOVERCLUSTER_H
#define AUTODISCOVERCLUSTER_H

#include "Cluster.h"
#include "Rpc.h"
#include <boost/date_time/posix_time/posix_time_duration.hpp>
#include <boost/smart_ptr/scoped_ptr.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/thread.hpp>


namespace fts {
namespace cluster {

class AutoDiscoverProcess;


/// Dynamically discover the network
class AutoDiscoverCluster: public Cluster {
public:
    static const boost::posix_time::time_duration kHeartBeatInterval;
    static const boost::posix_time::time_duration kPurgeTimeout;

private:
    boost::scoped_ptr<AutoDiscoverProcess> process;

public:
    AutoDiscoverCluster(Rpc::ptr_t rpc, bool isDummy = false);

    ~AutoDiscoverCluster();

    std::list<Node> getWorkers() const;
    std::list<Node> getMasters() const;

    Node getSelf() const;

    void setSelfType(Node::Type type);
};

} // end namespace cluster
} // end namespace fts

#endif // AUTODISCOVERCLUSTER_H
