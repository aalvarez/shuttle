/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CLUSTER_H
#define CLUSTER_H

#include <list>
#include <string>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/noncopyable.hpp>


namespace fts {
namespace cluster {

/// A node within the cluster
struct Node {
    uint64_t    beat; ///< Last beat
    std::string id;   ///< Node id
    enum Type {
        kLeader,    ///< Cluster leader
        kBackup,    ///< Backup for leadership
        kWorker     ///< Woker. Doesn't apply to leadership
    } type;

    std::string getRepr(void) const throw();
};


/// Abstraction of the FTS3 cluster
class Cluster: public boost::noncopyable {
public:
    typedef boost::shared_ptr<Cluster> ptr_t;

    virtual ~Cluster() {
    }

    virtual std::list<Node> getWorkers() const = 0;
    virtual std::list<Node> getMasters() const = 0;

    virtual Node getSelf() const = 0;

    virtual void setSelfType(Node::Type type) = 0;
};

} // end namespace cluster
} // end namespace fts

#endif // CLUSTER_H
