/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Raft.h"
#include <boost/random.hpp>
#include <boost/thread.hpp>
#include <common/Logger.h>


namespace fts {
namespace cluster {

static boost::random::mt19937 rng(time(NULL));

const boost::posix_time::time_duration Raft::kLeaderBeatInterval = boost::posix_time::seconds(2);
const boost::posix_time::time_duration Raft::kMinElectionTimeout = boost::posix_time::seconds(5);
const boost::posix_time::time_duration Raft::kMaxElectionTimeout = boost::posix_time::seconds(10);


class RaftProcess: public RaftListener {
private:
    boost::posix_time::time_duration electionTimeout;
    Cluster::ptr_t cluster;
    Rpc::ptr_t rpc;
    boost::thread_group threads;
    enum Status {
        kFollower,
        kCandidate,
        kLeader
    } status;
    unsigned currentTerm;
    unsigned votesFor, votesAgainst;
    std::string votedFor;
    std::string leader;

    time_t lastBeat;

public:
    RaftProcess(Cluster::ptr_t cluster, Rpc::ptr_t rpc):
        cluster(cluster), rpc(rpc), status(kFollower), currentTerm(0),
        votesFor(0), votesAgainst(0)
    {
        boost::random::uniform_int_distribution<> electionTimeoutDist(
            Raft::kMinElectionTimeout.total_milliseconds(), Raft::kMaxElectionTimeout.total_milliseconds()
        );
        electionTimeout = boost::posix_time::millisec(electionTimeoutDist(rng));

        // If we are dummy, do not run the election algorithm
        if (cluster->getSelf().type != Node::kWorker) {
            threads.create_thread(boost::bind(&RaftProcess::run, this));
            rpc->setRaftListener(this);
        }
    }

    ~RaftProcess() {
        stop();
    }

    void stop() {
        threads.interrupt_all();
        threads.join_all();
    }

    unsigned getQuorum(void) {
        return cluster->getMasters().size() / 2 + 1;
    }

    std::string getLeader(void) {
        return leader;
    }

    uint64_t getCurrentTerm(void) const {
        return currentTerm;
    }

    void run() {
        while (!boost::this_thread::interruption_requested()) {
            try {
                switch (status) {
                    case kFollower:
                        runFollower();
                        break;
                    case kCandidate:
                        runCandidate();
                        break;
                    case kLeader:
                        runLeader();
                        break;
                }
            }
            catch (const boost::thread_interrupted&) {
                return;
            }
        }
    }

    void runFollower() {
        time_t lastCheck;
        do {
            lastCheck = time(NULL);
            boost::this_thread::sleep(electionTimeout);
        } while (lastBeat > lastCheck);
        FTS3_COMMON_LOGGER_NEWLOG(WARNING) << "Follower time expired" << fts3::common::commit;
        beginElection();
    }

    void runCandidate() {
        FTS3_COMMON_LOGGER_NEWLOG(DEBUG)
            << "Wait for election with timeout " << electionTimeout.total_milliseconds()
            << fts3::common::commit;

        boost::this_thread::sleep(electionTimeout);
        switch (status) {
            case kCandidate:
                FTS3_COMMON_LOGGER_NEWLOG(WARNING) << "Election expired without a leader" << fts3::common::commit;
                beginElection();
                break;
            case kFollower:
                FTS3_COMMON_LOGGER_NEWLOG(INFO) << "Election finished, didn't win" << fts3::common::commit;
                break;
            case kLeader:
                FTS3_COMMON_LOGGER_NEWLOG(INFO) << "Election finished, I'm leader now" << fts3::common::commit;
                break;
        }
    }

    void runLeader() {
        // Hold the leadership as long as we are alive!
        while (!boost::this_thread::interruption_requested() && status == kLeader) {
            FTS3_COMMON_LOGGER_NEWLOG(TRACE) << "Sending leader ping" << fts3::common::commit;
            rpc->sendLeaderBeat(cluster->getSelf().id, currentTerm);
            boost::this_thread::sleep(Raft::kLeaderBeatInterval);
        }
        if (!boost::this_thread::interruption_requested()) {
            FTS3_COMMON_LOGGER_NEWLOG(WARNING) << "Someone took over!" << fts3::common::commit;
        }
    }

    void beginElection() {
        status = kCandidate;
        ++currentTerm;
        votedFor = cluster->getSelf().id;
        votesFor = 0;
        votesAgainst = 0;
        rpc->requestVoting(cluster->getSelf().id, currentTerm);
    }

    void notifyVoteRequest(const std::string &id, uint64_t term) {
        if (term < currentTerm) {
            FTS3_COMMON_LOGGER_NEWLOG(INFO)
                << "Not voted for " << id
                << " (" << term << ", " << currentTerm << ")"
                << fts3::common::commit;
            rpc->vote(id, false);
        }
        else if (votedFor.empty() || votedFor == id) {
            FTS3_COMMON_LOGGER_NEWLOG(INFO)
                << "Vote for " << id
                << " (" << term << ", " << currentTerm << ")"
                << fts3::common::commit;
            votedFor = id;
            rpc->vote(id, true);
        }
    }

    void notifyVote(const std::string &id, bool vote) {
        if (id != cluster->getSelf().id) {
            return;
        }
        else if (status != kCandidate) {
            return;
        }

        if (vote) {
            FTS3_COMMON_LOGGER_NEWLOG(INFO) << "Received vote for me" << fts3::common::commit;
        }
        else {
            FTS3_COMMON_LOGGER_NEWLOG(INFO) << "Received vote against me" << fts3::common::commit;
        }

        if (vote) {
            ++votesFor;
            if (votesFor >= getQuorum()) {
                FTS3_COMMON_LOGGER_NEWLOG(INFO) << "Got " << votesFor << ", so become leader" << fts3::common::commit;
                status = kLeader;
                rpc->sendLeaderBeat(cluster->getSelf().id, currentTerm);
                cluster->setSelfType(Node::kLeader);
                leader = cluster->getSelf().id;
            }
        }
        else {
            ++votesAgainst;
        }
    }

    void notifyLeaderBeat(const std::string &id, uint64_t term) {
        if (id == cluster->getSelf().id) {
            return;
        }

        FTS3_COMMON_LOGGER_NEWLOG(TRACE) << "Received leader ping from " << id << " " << term << fts3::common::commit;

        if (term > currentTerm) {
            currentTerm = term;
            status = kFollower;
            cluster->setSelfType(Node::kBackup);
            votedFor.clear();
            leader = id;
        }
        lastBeat = time(NULL);
    }
};



Raft::Raft(Cluster::ptr_t cluster, Rpc::ptr_t rpc): process(new RaftProcess(cluster, rpc))
{
}


Raft::~Raft()
{
}


std::string Raft::getLeader(void) const
{
    return process->getLeader();
}


uint64_t Raft::getCurrentTerm(void) const
{
    return process->getCurrentTerm();
}


void Raft::stop()
{
    process->stop();
}


} // end namespace cluster
} // end namespace fts
