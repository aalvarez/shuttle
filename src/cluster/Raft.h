/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAFT_H
#define RAFT_H

#include "Cluster.h"
#include "Rpc.h"
#include <boost/date_time/posix_time/posix_time_duration.hpp>
#include <boost/smart_ptr/scoped_ptr.hpp>
#include <boost/smart_ptr/shared_ptr.hpp>


namespace fts {
namespace cluster {

class RaftProcess;


class Raft: public boost::noncopyable {
public:
    static const boost::posix_time::time_duration kLeaderBeatInterval;
    static const boost::posix_time::time_duration kMinElectionTimeout;
    static const boost::posix_time::time_duration kMaxElectionTimeout;

private:
    boost::scoped_ptr<RaftProcess> process;

public:
    typedef boost::shared_ptr<Raft> ptr_t;

    Raft(Cluster::ptr_t cluster, Rpc::ptr_t rpc);
    ~Raft();

    std::string getLeader(void) const;
    uint64_t getCurrentTerm(void) const;

    void stop();
};

} // end namespace cluster
} // end namespace fts


#endif // SHUTTLE_RAFT_H
