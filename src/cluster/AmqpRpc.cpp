/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "AmqpRpc.h"
#include "HeartBeat.pb.h"
#include "LeaderBeat.pb.h"
#include "RequestVote.pb.h"
#include "Vote.pb.h"
#include <common/Logger.h>


namespace fts {
namespace cluster {


void AmqpRpc::serveCalls(void)
{
    while (!boost::this_thread::interruption_requested()) {
        try {
            AmqpClient::Envelope::ptr_t envelope = rpcConsumer->BasicConsumeMessage(rpcConsumerTag);

            if (envelope->RoutingKey() == "fts.cluster.node.heartbeat") {
                HeartBeat beat;
                beat.ParseFromString(envelope->Message()->Body());

                Node node;
                node.beat = beat.beat();
                node.id = beat.id();
                switch (beat.type()) {
                    case HeartBeat_Type_BACKUP:
                        node.type = Node::kBackup;
                        break;
                    case HeartBeat_Type_LEADER:
                        node.type = Node::kLeader;
                        break;
                    default:
                        node.type = Node::kWorker;
                }

                if (clusterDiscovery) {
                    clusterDiscovery->notifyNode(node);
                }
            }
            else if (envelope->RoutingKey() == "fts.cluster.raft.requestVote") {
                RequestVote request;
                request.ParseFromString(envelope->Message()->Body());
                if (raft) {
                    raft->notifyVoteRequest(request.candidateid(), request.term());
                }
            }
            else if (envelope->RoutingKey() == "fts.cluster.raft.vote") {
                Vote vote;
                vote.ParseFromString(envelope->Message()->Body());
                if (raft) {
                    raft->notifyVote(vote.candidateid(), vote.votegranted());
                }
            }
            else if (envelope->RoutingKey() == "fts.cluster.raft.leader") {
                LeaderBeat beat;
                beat.ParseFromString(envelope->Message()->Body());
                if (raft) {
                    raft->notifyLeaderBeat(beat.id(), beat.term());
                }
            }
            else {
                FTS3_COMMON_LOGGER_NEWLOG(WARNING) << "Unknown call " << envelope->RoutingKey() << fts3::common::commit;
            }
        }
        catch (const boost::thread_interrupted&) {
            break;
        }
        catch (...) {
            // Pass
        }
    }
}


AmqpRpc::AmqpRpc(const AmqpConnectionParameters &params)
{
    rpcProducer = AmqpClient::Channel::Create(
        params.host, params.port, params.username, params.password, params.vhost);
    rpcConsumer = AmqpClient::Channel::Create(
        params.host, params.port, params.username, params.password, params.vhost);

    rpcProducer->DeclareExchange(
        "fts.e.cluster", AmqpClient::Channel::EXCHANGE_TYPE_FANOUT, false, false, true);
    rpcConsumer->DeclareExchange(
        "fts.e.cluster", AmqpClient::Channel::EXCHANGE_TYPE_FANOUT, false, false, true);

    std::string rpcQueueName = rpcConsumer->DeclareQueue(std::string());
    rpcConsumer->BindQueue(rpcQueueName, "fts.e.cluster");
    rpcConsumerTag = rpcConsumer->BasicConsume(rpcQueueName, "");

    consumerThread.reset(new boost::thread(boost::bind(&AmqpRpc::serveCalls, this)));
}


AmqpRpc::~AmqpRpc()
{
    consumerThread->interrupt();
    consumerThread->join();
}


void AmqpRpc::sendHeartbeat(const Node &node)
{
    boost::lock_guard<boost::mutex> lock(mutex);

    HeartBeat beat;
    beat.set_id(node.id);
    HeartBeat_Type type;
    switch (node.type) {
        case Node::kLeader:
            type = HeartBeat_Type_LEADER;
            break;
        case Node::kBackup:
            type = HeartBeat_Type_BACKUP;
            break;
        default:
            type = HeartBeat_Type_WORKER;
    }
    beat.set_type(type);
    beat.set_beat(node.beat);

    AmqpClient::BasicMessage::ptr_t message(AmqpClient::BasicMessage::Create(beat.SerializeAsString()));
    message->Timestamp(time(NULL));
    rpcProducer->BasicPublish("fts.e.cluster", "fts.cluster.node.heartbeat", message, true, false);
}


void AmqpRpc::sendLeaderBeat(const std::string &id, uint64_t term)
{
    boost::lock_guard<boost::mutex> lock(mutex);

    LeaderBeat beat;
    beat.set_id(id);
    beat.set_term(term);

    AmqpClient::BasicMessage::ptr_t message(AmqpClient::BasicMessage::Create(beat.SerializeAsString()));
    message->Timestamp(time(NULL));
    rpcProducer->BasicPublish("fts.e.cluster", "fts.cluster.raft.leader", message, true, false);
}


void AmqpRpc::requestVoting(const std::string &id, uint64_t term)
{
    boost::lock_guard<boost::mutex> lock(mutex);

    RequestVote request;
    request.set_term(term);
    request.set_candidateid(id);

    AmqpClient::BasicMessage::ptr_t message(AmqpClient::BasicMessage::Create(request.SerializeAsString()));
    message->Timestamp(time(NULL));
    rpcProducer->BasicPublish("fts.e.cluster", "fts.cluster.raft.requestVote", message, true, false);
}


void AmqpRpc::vote(const std::string &id, bool voteValue)
{
    boost::lock_guard<boost::mutex> lock(mutex);

    Vote vote;
    vote.set_votegranted(voteValue);
    vote.set_candidateid(id);

    AmqpClient::BasicMessage::ptr_t message(AmqpClient::BasicMessage::Create(vote.SerializeAsString()));
    message->Timestamp(time(NULL));
    rpcProducer->BasicPublish("fts.e.cluster", "fts.cluster.raft.vote", message, true, false);
}

} // end namespace cluster
} // end namespace fts
