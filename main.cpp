#include <iostream>
#include <notifications/Notifications.h>
#include <externals/SimpleAmqpClient/src/SimpleAmqpClient/SimpleAmqpClient.h>


class MonitoringProducer {
private:
    std::unique_ptr<fts3::Notifications> notifier;

public:

    MonitoringProducer() {
        fts3::NotificationParameters parameters;
        parameters.spoolDirectory = "/tmp/spoolmon";
        parameters.destinationAreTopics = true;
        parameters.startDestination = "start";
        parameters.stateDestination = "state";
        parameters.terminalDestination = "terminal";
        notifier.reset(new fts3::Notifications(parameters));
    }

    void shove(const fts3::events::TransferCompleteEvent &event)
    {
        try {
            notifier->sendTransferTerminal(event);
        }
        catch (...) {
            std::cout << "oops" << std::endl;
        }
    }
};

int main()
{
    // fts3 set up its consumer
    AmqpClient::Channel::ptr_t consumer = AmqpClient::Channel::Create("localhost", 5672);
    consumer->DeclareExchange("fts.e.url_copy", "amqp.topic", true, false, false);
    consumer->DeclareQueue("fts.q.transfer.updates");
    consumer->BindQueue("fts.q.transfer.updates", "fts.e.url_copy", "fts.transfer.#");
    std::string consumerTag = consumer->BasicConsume("fts.q.transfer.updates", "", true, true, true);

    // url copy would generate a message
    fts3::events::TransferCompleteEvent event;
    event.set_timestamp(time(NULL) * 1000);
    event.mutable_agent()->set_endpoint("fts3-devel.cern.ch");
    event.mutable_agent()->set_fqdn("fts3devel01.cern.ch");
    event.set_transfer_id("abcdef-01-1250");
    event.set_vo_name("dteam");
    event.set_user_dn("/DN=test");
    event.mutable_pair()->set_name("abcd_efgh");
    event.mutable_pair()->set_type("BLAH");
    event.mutable_pair()->mutable_source()->set_site_name("UK");
    event.mutable_pair()->mutable_source()->set_hostname("blahbleh");
    event.mutable_pair()->mutable_source()->set_surl("mock://blahbleh/path");
    event.mutable_pair()->mutable_destination()->set_site_name("ES");
    event.mutable_pair()->mutable_destination()->set_hostname("xyz");
    event.mutable_pair()->mutable_destination()->set_surl("mock://xyz/file");
    event.mutable_transfer()->set_start(time(NULL) - 100);
    event.mutable_transfer()->set_end(time(NULL) + 100);
    event.mutable_total_process()->set_start(time(NULL) - 100);
    event.mutable_total_process()->set_end(time(NULL) + 100);
    event.set_transfer_timeout(1000);
    event.set_checksum_timeout(1000);
    event.set_job_state("UNKNOWN");
    event.set_final_transfer_state("Ok");
    event.set_file_size(1024);
    event.set_total_bytes_transfered(1024);
    event.set_number_of_streams(1);
    event.set_tcp_buffer_size(0);
    event.set_block_size(0);
    event.set_retry(0);
    event.set_retry_max(1);
    event.set_is_job_multiple_replica(false);
    event.set_is_recoverable(false);

    AmqpClient::Channel::ptr_t producer = AmqpClient::Channel::Create("localhost", 5672);
    producer->DeclareExchange("fts.e.url_copy", "amqp.topic", true, false, false);
    producer->BasicPublish(
        "fts.e.url_copy", "fts.transfer.end",
        AmqpClient::BasicMessage::Create(event.SerializeAsString()),
        true, false
    );

    // fts3 would be running this
    AmqpClient::Envelope::ptr_t envelope = consumer->BasicConsumeMessage(consumerTag);
    fts3::events::TransferCompleteEvent event2;
    event2.ParseFromString(envelope->Message()->Body());
    std::cout << envelope->RoutingKey() << ": " << event2.DebugString() << std::endl;
    MonitoringProducer().shove(event2);
}
