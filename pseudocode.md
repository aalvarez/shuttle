Pseudocode
==========

## Naming conventions
* Dots separate name components
* All names start with `fts`
* Exchanges are specified with an `e` and queues with `q`
* The following component should identify a subsystem, or component, ...
* Additional components can be used to pinpoint the type of messages that go there

For instance:

* `fts.e.url_copy` is the exchange point used by fts_url_copy
* `fts.q.url_copy.start` is a queue used by FTS to consume start messages coming from fts_url_copy
* `fts.q.sched.slots` is a queued used by the scheduler to announce free slots

Similarly, for routing keys, the components should be representative. i.e `fts.transfer.start` vs `fts.transfer.end`

## For Url Copy
1. FTS binds one queue to exchange `fts.e.url_copy`
    1. For routing `fts.transfer.start`, update DB and send message to ActiveMQ
    1. For `fts.transfer.ping`, note process is alive
    1. For `fts.transfer.log`, update DB
    1. For `fts.transfer.end`, update DB and send message to ActiveMQ
1. Url copy sends messages to exchange `fts.e.url_copy`

Optionally:

1. Optimizer binds a queue `fts.optimizer.transfers_end` for routing `fts.transfer.end` and exchange point
`fts.e.url_copy`


## Scheduling / Optimizer / Queues
We can try to use messaging for the queues as well. We need at least a queue for pair, vo and (optionally) activity 
share.

The scheduler notifies workers of free slots when available. For instance, it can bind a queue
`fts.q.sched.transfer.end` to `fts.e.url_copy` with routing `fts.transfer.end`. Once it gets notified of a free slot,
it sends a "ticket" saying there has been an "opening" so a worker picks it up, go to the relevant queue, get a
job and run it.
 
These "jobs" are basically "bundles" of file ids, and the kind of transfer: multihop, reuse, ...
The workers need to make sure the job hasn't been cancelled before running the url_copy.
 
The scheduler knows how many transfers can be running in parallel depending on the configuration and/or optimizer 
output.
So the optimizer needs to share this number somehow. Database, probably.

Need to think what happens if messages get lost/duplicated. Once it goes into Rabbitmq, it will do its best to give 
the message to someone else. But it could happen

1. The producer fails to send the message to the broker
2. The broker gives the message to someone, this someone marks the message as consumed, but it fails to do something.
3. The broker gives the message to someone, this someone does the action but fails to ACK. The broker will eventually resubmit.

Therefore, actions should be idempotent if possible, or ok to lose.

## Messages

### Available slot
Not terrible either duplication or loss, unless it is systematic. Either way is just a small deviation that 
eventually will be corrected.
Still, in both cases there will be an inconsistency between the real number of transfers running and the accounting, 
but we can probably live with it as long as these inconsistencies are softened during the run time.

Probably both ACK and No Ack are fine.

### State change
* If lost, the database view of the status will be inconsistent
* If duplicated, as long as the transaction is idempotent (FINISHED => FINISHED) all is good
* If out of order, we need to make sure at the DB level we don't go from FINISHED to ACTIVE, for instance
    * Beware! Timestamp is very relevant here, as the ACTIVE could be an actual duplicated run
    
Therefore, using ACK for these looks all better

### Terminal message
* If lost, clients will not see them, although the optimizer can live with missing messages. The scheduler wouldn't 
re-schedule another one.
* Duplication is idempotent
    * Someone may complain, but we can only guarantee at least once, or not more than once. Exactly once is an 
    impossibility. Since receiving twice the same final state is idempotent, we better go for at least once.
* Out of order irrelevant

Requiring ACK looks like it.

### New task for a worker
* If lost, the task will remain in the queue forever. Need to expire stalled jobs.
    * Once marked as canceled, if retrieved later, it will not be processed
* Out of order should be fine.
* If duplicated, we risk runnig twice the same transfer. So either
    * Require ACK, but require as well going through the DB to make sure it wasn't already picked
    * Do not require ACK, let it be lost
    
Spawning url_copy is not trivial and may fail. If this is due to constrained resources, etc... we need to make sure 
we do not lose a lot of taks.
Spawning successfully, but failing before/during the ACK, is way way riskier if it triggers twice the same transfer.

So No ACK for these sounds better, and either mark as failed or re-queue if the spawning fails. If can not do because
 the server crashes before it has the chance, then the task will be lost. Need to be able to recover these.
 
 
### In summary

* Available slot: Not more than once, or at least once, either way
* State change: At least once
* Terminal message: At least once
* Tasks: Not more than once

