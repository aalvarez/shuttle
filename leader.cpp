/* Run as many was wanted, kill them at will */
#include <iostream>
#include <set>

#include <cluster/AmqpRpc.h>
#include <cluster/AutoDiscoverCluster.h>
#include <cluster/Raft.h>


int main(int argc, char *argv[])
{
    fts::cluster::AmqpConnectionParameters connParams;
    bool isDummy = false;
    connParams.host = "localhost";
    connParams.port = 5672;
    connParams.username = "guest";
    connParams.password = "guest";
    connParams.vhost = "/";

    if (argc > 1) {
        isDummy = atoi(argv[1]);
    }
    if (argc > 2) {
        connParams.host = argv[2];
    }
    if (argc > 3) {
        connParams.port = atoi(argv[3]);
    }
    if (argc > 4) {
        connParams.username = argv[4];
    }
    if (argc > 5) {
        connParams.password = argv[5];
    }
    if (argc > 6) {
        connParams.vhost = argv[6];
    }

    fts::cluster::Rpc::ptr_t rpc(new fts::cluster::AmqpRpc(connParams));
    fts::cluster::Cluster::ptr_t cluster(new fts::cluster::AutoDiscoverCluster(rpc, isDummy));
    fts::cluster::Raft::ptr_t raft(new fts::cluster::Raft(cluster, rpc));

    std::cerr << cluster->getSelf().id << std::endl;

    while (!boost::this_thread::interruption_requested()) {
        /*
        std::cerr << "* " << time(NULL) << std::endl;
        auto members = cluster->getMasters();
        for (auto i = members.begin(); i != members.end(); ++i) {
            std::cerr << "\t" << i->getRepr() << std::endl;
        }
         */
        boost::this_thread::sleep(boost::posix_time::seconds(1));
    }

    return 0;
}
