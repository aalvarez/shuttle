/*
 * Copyright (c) CERN 2016
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <iostream>
#include <msg-bus/events/TransferCompleteEvent.pb.h>
#include <msg-bus/events/TransferStartEvent.pb.h>
#include <msg-bus/events/TransferPingEvent.pb.h>
#include <msg-bus/events/TransferLogEvent.pb.h>
#include <externals/SimpleAmqpClient/src/SimpleAmqpClient/SimpleAmqpClient.h>



void dumpUnknown(const AmqpClient::Envelope::ptr_t &envelope)
{
    std::cout << "## Unexpected message: exchange " << envelope->Exchange()
        << " / key " << envelope->RoutingKey()
        << std::endl;
}

void dumpStart(const AmqpClient::Envelope::ptr_t &envelope)
{
    std::cout << "## Transfer start" << std::endl;
    fts3::events::TransferStartEvent event;
    event.ParseFromString(envelope->Message()->Body());
    std::cout << event.DebugString() << std::endl;
}

void dumpEnd(const AmqpClient::Envelope::ptr_t &envelope)
{
    std::cout << "## Transfer end" << std::endl;
    fts3::events::TransferCompleteEvent event;
    event.ParseFromString(envelope->Message()->Body());
    std::cout << event.DebugString() << std::endl;
}

void dumpPing(const AmqpClient::Envelope::ptr_t &envelope)
{
    std::cout << "## Transfer ping" << std::endl;
    fts3::events::TransferPingEvent event;
    event.ParseFromString(envelope->Message()->Body());
    std::cout << event.DebugString() << std::endl;
}

void dumpLog(const AmqpClient::Envelope::ptr_t &envelope)
{
    std::cout << "## Transfer log" << std::endl;
    fts3::events::TransferLogEvent event;
    event.ParseFromString(envelope->Message()->Body());
    std::cout << event.DebugString() << std::endl;
}


void dump(const AmqpClient::Envelope::ptr_t &envelope)
{
    if (envelope->Exchange() != "fts.e.url_copy") {
        dumpUnknown(envelope);
    }
    else if (envelope->RoutingKey() == "fts.transfer.start") {
        dumpStart(envelope);
    }
    else if (envelope->RoutingKey() == "fts.transfer.end") {
        dumpEnd(envelope);
    }
    else if (envelope->RoutingKey() == "fts.transfer.ping") {
        dumpPing(envelope);
    }
    else if (envelope->RoutingKey() == "fts.transfer.log") {
        dumpLog(envelope);
    }
    else {
        dumpUnknown(envelope);
    }
}


int main(int argc, char *argv[])
{
    std::string host = "localhost";
    unsigned port = 5672;
    std::string username = "guest";
    std::string password = "guest";
    std::string vhost = "/";

    if (argc > 1) {
        host = argv[1];
    }
    if (argc > 2) {
        port = atoi(argv[2]);
    }
    if (argc > 3) {
        username = argv[3];
    }
    if (argc > 4) {
        password = argv[4];
    }
    if (argc > 5) {
        vhost = argv[5];
    }

    AmqpClient::Channel::ptr_t connection = AmqpClient::Channel::Create(host, port, username, password, vhost);

    connection->DeclareExchange("fts.e.url_copy", "amqp.topic", true, false, false);
    connection->DeclareQueue("fts.q.debug", false, false, true, true);
    connection->BindQueue("fts.q.debug", "fts.e.url_copy", "fts.transfer.#");

    std::string consumerTag = connection->BasicConsume("fts.q.debug", "", true, true, true);

    while (true) {
        AmqpClient::Envelope::ptr_t envelope = connection->BasicConsumeMessage(consumerTag);
        dump(envelope);
    }

    return 0;
}
